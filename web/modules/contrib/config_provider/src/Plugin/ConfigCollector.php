<?php

namespace Drupal\config_provider\Plugin;

use Drupal\config_provider\InMemoryStorage;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Config\ConfigManagerInterface;
use Drupal\Core\Config\StorageInterface;

/**
 * Class for invoking configuration providers..
 */
class ConfigCollector implements ConfigCollectorInterface {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The active configuration storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $activeStorage;

  /**
   * The configuration manager.
   *
   * @var \Drupal\Core\Config\ConfigManagerInterface
   */
  protected $configManager;

  /**
   * The provider configuration storage.
   *
   * @var \Drupal\Core\Config\StorageInterface
   */
  protected $providerStorage;

  /**
   * The configuration provider manager.
   *
   * @var \Drupal\config_provider\Plugin\ConfigProviderManager
   */
  protected $configProviderManager;

  /**
   * The configuration provider plugin instances.
   *
   * @var \Drupal\config_provider\Plugin\ConfigProvider
   */
  protected $configProviders;

  /**
   * Constructor for ConfigCollector objects.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\Core\Config\StorageInterface $active_storage
   *   The active configuration storage.
   * @param \Drupal\Core\Config\ConfigManagerInterface $config_manager
   *   The configuration manager.
   * @param \Drupal\Core\Config\StorageInterface $provider_storage
   *   The provider configuration storage.
   * @param \Drupal\config_provider\Plugin\ConfigProviderManager $config_provider_manager
   *   The configuration provider manager.
   */
  public function __construct(ConfigFactoryInterface $config_factory, StorageInterface $active_storage, ConfigManagerInterface $config_manager, StorageInterface $provider_storage, ConfigProviderManager $config_provider_manager) {
    $this->configFactory = $config_factory;
    $this->activeStorage = $active_storage;
    $this->configManager = $config_manager;
    $this->providerStorage = $provider_storage;
    $this->configProviderManager = $config_provider_manager;
    $this->configProviders = [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfigProviders() {
    if (empty($this->configProviders)) {
      $definitions = $this->configProviderManager->getDefinitions();
      foreach (array_keys($definitions) as $id) {
        $this->initConfigProviderInstance($id);
      }
    }
    return $this->configProviders;
  }

  /**
   * {@inheritdoc}
   */
  public function getInstallableConfig(array $extensions = []) {
    // Start with an empty storage.
    $this->providerStorage->deleteAll();

    /* @var \Drupal\config_provider\Plugin\ConfigProviderInterface[] $providers */
    $providers = $this->getConfigProviders();

    foreach ($providers as $provider) {
      $provider->addInstallableConfig($this->providerStorage, $extensions);
    }

    return $this->providerStorage;
  }

  /**
   * Initializes an instance of the specified configuration provider.
   *
   * @param string $id
   *   The string identifier of the configuration provider.
   */
  protected function initConfigProviderInstance($id) {
    if (!isset($this->configProviders[$id])) {
      $instance = $this->configProviderManager->createInstance($id, []);
      $instance->setConfigFactory($this->configFactory);
      $instance->setActiveStorages($this->activeStorage);
      $instance->setConfigManager($this->configManager);
      $this->configProviders[$id] = $instance;
    }
  }

}

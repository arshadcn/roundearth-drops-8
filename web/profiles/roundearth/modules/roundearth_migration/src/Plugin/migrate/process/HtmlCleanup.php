<?php

namespace Drupal\roundearth_migration\Plugin\migrate\process;

use Drupal\Component\Utility\Html;
use Drupal\Core\Config\ImmutableConfig;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\migrate\MigrateExecutableInterface;
use Drupal\migrate\ProcessPluginBase;
use Drupal\migrate\Row;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Migrate process plugin for cleaning up HTML content.
 *
 * @MigrateProcessPlugin(
 *   id = "roundearth_migration_html_cleanup"
 * )
 */
class HtmlCleanup extends ProcessPluginBase implements ContainerFactoryPluginInterface {

  /**
   * Default configuration values.
   *
   * @var array
   */
  protected $defaultConfig = [
    'html_cleanup_styles',
  ];

  /**
   * The migration, or NULL if not available.
   *
   * @var \Drupal\migrate\MigrateExecutableInterface|null
   */
  protected $migrateExecutable;

  /**
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $settings;

  /**
   * EmbeddedMedia constructor.
   *
   * @param array $configuration
   * @param string $plugin_id
   * @param mixed $plugin_definition
   * @param \Drupal\Core\Config\ImmutableConfig $settings
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, ImmutableConfig $settings) {
    $this->settings = $settings;
    parent::__construct($configuration, $plugin_id, $plugin_definition);
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('config.factory')->get('roundearth_migration.settings')
    );
  }

  /**
   * Get plugin configuration.
   *
   * @return array
   *   Configuration values.
   */
  protected function getConfig() {
    $config = $this->configuration;
    foreach ($this->defaultConfig as $key) {
      if (empty($config[$key])) {
        $config[$key] = $this->settings->get($key);
      }
    }
    return $config;
  }

  /**
   * {@inheritdoc}
   */
  public function transform($value, MigrateExecutableInterface $migrate_executable, Row $row, $destination_property) {
    $this->migrateExecutable = $migrate_executable;
    $document = Html::load($value);
    $this->processHtml($document);
    $value = Html::serialize($document);
    return $value;
  }

  /**
   * Process and clean-up HTML.
   *
   * @param \DOMNode $node
   *   The DOM node to process.
   */
  protected function processHtml(\DOMNode $node) {
    if ($node instanceof \DOMElement) {
      $config = $this->getConfig();
      if (!empty($config['html_cleanup_styles']) && is_array($config['html_cleanup_styles']) && $node->hasAttribute('style')) {
        $attr = $orig_attr = $node->getAttribute('style');
        foreach ($config['html_cleanup_styles'] as $style) {
          $attr = trim(preg_replace('/' . preg_quote($style) . ':[^;$]+;?/', '', $attr));
        }
        if ($attr !== $orig_attr) {
          $node->setAttribute('style', $attr);
        }
      }
    }

    if (!empty($node->childNodes)) {
      foreach ($node->childNodes as $child) {
        $this->processHtml($child);
      }
    }
  }

}
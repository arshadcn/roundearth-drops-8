<?php

use Drupal\Core\Form\FormStateInterface;

/**
 * @file
 * Theme settings
 */

/*
 * Implements hook_form_system_theme_settings_alter()
 */
function pangea_form_system_theme_settings_alter(&$form, &$form_state) {
  // Hide page element display settings.
  $form['theme_settings']['#access'] = FALSE;

  $form['layout_settings'] = [
    '#title' => t('Layout'),
    '#type' => 'fieldset',
    '#collapsed' => FALSE,
  ];

  $form['layout_settings']['container'] = [
    '#type' => 'radios',
    '#title' => t('Container'),
    '#default_value' => theme_get_setting('container'),
    '#description' => t('Choose from a responsive, fixed-width container or fluid-width.'),
    '#options' => [
      'fixed' => t('Fixed'),
      'fluid' => t('Fluid'),
    ],
  ];

  // Footer settings.
  $form['footer_settings'] = [
    '#title' => t('Footer'),
    '#description' => t('Customize the site footer.'),
    '#type' => 'details',
    '#open' => TRUE,
  ];

  $form['footer_settings']['copyright'] = [
    '#type' => 'textfield',
    '#title' => t('Copyright'),
    '#description' => t('Enter the copyright to display in the footer. You can use <em>@year</em> for current year. Leave blank for no copyright.', [
      '@year' => '[YEAR]',
    ]),
    '#default_value' => theme_get_setting('copyright'),
  ];

  // Add a custom submit handler.
  $form['#submit'][] = 'pangea_form_system_theme_settings_submit';
}

/**
 * Custom submit handler for Pangea settings form.
 */
function pangea_form_system_theme_settings_submit($form, FormStateInterface $form_state) {
  // Clear the cache.
  drupal_flush_all_caches();
}

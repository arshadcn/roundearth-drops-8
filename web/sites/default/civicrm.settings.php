<?php

global $base_path, $base_url, $civicrm_root, $civicrm_setting;

// Settings to apply everywhere, override as needed.
$civicrm_setting['Developer Preferences']['environment'] = 'Development';
$civicrm_setting['Developer Preferences']['assetCache'] = '0';

/**
 * Pantheon settings.
 */
if (!empty($_SERVER['PRESSFLOW_SETTINGS'])) {
  $env = json_decode($_SERVER['PRESSFLOW_SETTINGS'], TRUE);
  if (!empty($env['conf']['pantheon_binding'])) {
    $pantheon_db = $env['databases']['default']['default'];
    $pantheon_conf = $env['conf'];

    //user name and password
    $db_string = $pantheon_db['driver'] . '://' . $pantheon_db['username'] . ':' . $pantheon_db['password'] . '@';
    //host
    $db_string .= $pantheon_db['host'] . ':' . $pantheon_db['port'];
    // database
    $db_string .= '/' . $pantheon_db['database'] . '?new_link=true';

    // define the database strings
    define('CIVICRM_UF_DSN', $db_string);
    define('CIVICRM_DSN', $db_string);

    if ($pantheon_conf['pantheon_binding'] === 'lando') {
      $civicrm_root = $_SERVER['LANDO_MOUNT'] . '/vendor/civicrm/civicrm-core';
      define('CIVICRM_TEMPLATE_COMPILEDIR', $_SERVER['LANDO_WEBROOT'] . '/' . $pantheon_conf['file_private_path'] . '/civicrm/templates_c/');
    }
    else {
      $civicrm_root = '/srv/bindings/' . $pantheon_conf['pantheon_binding'] . '/code/vendor/civicrm/civicrm-core';
      define('CIVICRM_TEMPLATE_COMPILEDIR', '/srv/bindings/' . $pantheon_conf['pantheon_binding'] . '/files/private/civicrm/templates_c/');

      // For our custom hack to move just the templates_c directory to a special place.
      define('CIVICRM_REAL_TEMPLATE_COMPILEDIR', $_SERVER['PANTHEON_ROLLING_TMP'] . '/civicrm/templates_c/');
    }

    // Put the image upload URL in the public filesystem.
    $civicrm_setting['domain']['imageUploadDir'] = '[cms.root]/sites/default/files/civicrm/persist/contribute/';

    // Use Drupal base url and path
    define('CIVICRM_UF_BASEURL', $base_url . '/');

    // Use Redis cache.
    if (!empty($pantheon_conf['redis_client_password'])) {
      define( 'CIVICRM_DB_CACHE_CLASS', 'Redis' );
      define( 'CIVICRM_DB_CACHE_HOST', $pantheon_conf['redis_client_host'] );
      define( 'CIVICRM_DB_CACHE_PORT', $pantheon_conf['redis_client_port'] );
      define( 'CIVICRM_DB_CACHE_PASSWORD', $pantheon_conf['redis_client_password'] );
      define( 'CIVICRM_DB_CACHE_TIMEOUT', 3600 );
      define( 'CIVICRM_DB_CACHE_PREFIX', 'pantheon-redis-civicrm:' );
    }
  }
}

/**
 * Local settings.
 */
if (file_exists(__DIR__ . '/civicrm.local.inc')) {
  require_once(__DIR__ . '/civicrm.local.inc');
}

/**
 * The upstream CiviCRM settings.
 */
require_once(__DIR__ . '/civicrm.settings.inc');


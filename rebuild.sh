#!/bin/bash

set -e

: ${COMPOSER:=composer}

original_dir=`pwd`
working_dir=`mktemp -d 2>/dev/null || mktemp -d -t 'roundearth'`
cd $working_dir

# Grab the template.
git clone --depth=1 --branch=8.x https://gitlab.com/roundearth/roundearth-composer-template.git build
rm -rf !$/.git

# Build the profile
cd build
composer install
composer drupal:scaffold

# Copy back here (skipping the files that we copied from here)
cp $working_dir/build/composer.{lock,json} $original_dir
rsync -ma --delete --exclude=.git/ $working_dir/build/vendor $original_dir
rsync -ma --delete --exclude=.git --exclude=/.gitignore --exclude=/profiles/roundearth --exclude=/sites --exclude='/.*' --include=/libraries/civicrm --exclude='/libraries/*' --exclude=/modules/custom --exclude=/themes/pangea_custom $working_dir/build/web/ $original_dir/web

# Clean-up the build
#echo "Build at: $working_dir/build"
rm -rf $working_dir


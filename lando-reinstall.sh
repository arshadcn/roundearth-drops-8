#!/bin/bash

#set -e

lando drush sqlq "SET FOREIGN_KEY_CHECKS=0; DROP DATABASE pantheon; CREATE DATABASE pantheon;"
rm -rf web/sites/default/files/*
chmod +w web/sites/default

exec ./lando-install.sh

